#!/usr/bin/env bash

set -euo pipefail

DIR="$(cd "$(dirname "$0")" ; pwd -P)"

# CI_COMMIT_REF_NAME indicates that `do` is run in a CI pipeline in gilab
export TEST_POSTFIX="${CI_COMMIT_REF_NAME:-${USER:-ci}}"

function task_test {
  TODO
}

function task_usage {
  cat <<-eof
Usage: ./do [command], where command is one of:

  bootstrap                       TODO
  test                            run unit tests

eof
  exit 1
}

ARG=${1:-}
shift || true

case ${ARG} in
  bootstrap) bootstrap_venv "$@" ;;
  run) task_run "$@" ;;
  test) task_test "$@" ;;
  *) task_usage ;;
esac

